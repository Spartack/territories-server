const Territoire = require("../models/Territoire");
const Demand = require("../models/Demand");
const User = require("../models/User");

///////////////////////////////////////////////// GET /////////////////////////////////////////////

// get all territoires
// @route GET /API/territoires
exports.getTerritoires = async (req, res, next) => {
  try {
    const territoires = await Territoire.find().sort({ territoireID: 1 });

    return res.status(200).json({
      success: true,
      count: territoires.length,
      data: territoires,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "server error" });
  }
};

// get territoire by ID
// @route GET /API/territoires/:id
exports.getTerritoireById = async (req, res) => {
  try {
    console.log("get by id");

    const territoire = await Territoire.findOne({
      _id: req.params.id,
    });

    res.status(200).json({
      success: true,
      data: territoire,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "server error" });
  }
};

exports.getFreeTerritories = async (req, res) => {
  try {
    const territories = await Territoire.find({ assigned: false }).sort({
      territoireID: 1,
    });
    res
      .status(200)
      .json({ success: true, count: territories.length, data: territories });
  } catch (error) {
    res.status(400).json({ succss: false, error });
  }
};

///////////////////////////////////////////// PUT ////////////////////////////////////////////

//modifie le territoire dont l'id est dans les params par l'objet en req.body
// @route PUT API/territoires/:id
exports.updateOneTerritoireById = async (req, res, next) => {
  try {
    const territoire = await Territoire.updateOne(
      { _id: req.params.id },

      { ...req.body }
    );

    res.status(200).json({
      success: true,
      data: territoire,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "server error" });
  }
};
//////////////////////////////////////// POST ///////////////////////////////////////////////

exports.addOneTerritoire = async (req, res, next) => {
  territoireToSave = req.body;

  await Territoire.insertMany([territoireToSave], function (
    err,
    territoireToSave
  ) {
    if (err) return res.send({ err });
    res.json({ territoireToSave });
  });
};

// create a territoire qui rentre un fichier geojson en parametre req.
//insert dans la base l'Id automatique, le nom si renseigné dans le fichier geojson dans properties
// @route POST /API/territoires
exports.addTerritoires = async (req, res, next) => {
  try {
    // Init tableau qui prendra des objets de notre schema Territoires à insérer par la suite
    polygonsToInsert = [];
    // loop sur le tableau avec index qui commence à  1 et qui s'incrémente au fur et a mesure
    let index = (await Territoire.find().count()) + 1;

    req.body.features.forEach((feature) => {
      const name = feature.properties.name ? feature.properties.name : index;
      const quartier = feature.properties.quartier
        ? feature.properties.quartier
        : null;
      territoireToAdd = {
        territoireID: index,
        territoireName: name,
        coordinates: feature.geometry.coordinates,
      };
      polygonsToInsert.push(territoireToAdd);
      index++;
    });

    await Territoire.insertMany(polygonsToInsert, (err, docs) => {
      if (err) {
        return console.error(err);
      } else {
        // si l'insertion marche
        console.log(index + " documents inserted to Collection");
      }
    });

    res.status(200).json({
      success: true,
      lenght: polygonsToInsert.lenght - 1,
      data: polygonsToInsert,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "server error" });
  }
};

/////////////////////////////////// DELETE //////////////////////////////////////////
// delete territoire en fonction de l'id en params
// @route DELETE /API/territoires/:id
exports.deleteTerritoireById = async (req, res, next) => {
  try {
    await Territoire.deleteOne({
      territoireID: req.params.id,
    });

    return res.status(200).json({
      success: true,
      message: "territoire supprimé",
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "server error" });
  }
};

// delete tous les territoire de la collection
// @route DELETE /API/territoires/
exports.deleteAllTerritoires = async (req, res, next) => {
  try {
    console.log("delete all");
    await Territoire.remove();

    return res.status(200).json({
      success: true,
      message: "Toute la base est supprimée",
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "server error" });
  }
};
