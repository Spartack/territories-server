const User = require("../models/User");
const Territoire = require("../models/Territoire");
const Demand = require("../models/Demand");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const fs = require("fs");

exports.signup = async (req, res, next) => {
  try {
    hash = await bcrypt.hash(req.body.password, 10);

    userToSave = new User({
      email: req.body.email,
      password: hash,
      name: req.body.name,
      firstname: req.body.firstname,
      admin: req.body.admin,
    });

    userToSave
      .save()
      .then(
        res.status(201).json({ success: true, message: "utilisateur créé" })
      )
      .catch((error) => res.status(400).json({ success: false, error }));
  } catch (error) {
    res.status(500).json({ success: false, error });
  }
};

exports.login = async (req, res, next) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (user != null) {
      bcrypt.compare(req.body.password, user.password).then((valid) => {
        if (!valid)
          res
            .status(401)
            .json({ success: false, error: "Mot de passe incorrect" });
        res.status(200).json({
          user: {
            email: user.email,
            id: user._id,
            name: user.name,
            firstname: user.firstname,
            admin: user.admin,
            token: jwt.sign({ id: user._id }, "RANDOM_TOKEN_SECRET", {
              expiresIn: "1h",
            }),
          },
        });
      });
    } else {
      res.status(401).json({ success: false, error: "utilisateur non trouvé" });
    }
  } catch (error) {
    res.status(500).json({ success: false, error: error });
  }
};

exports.demandTerritoire = async (req, res) => {
  try {
    const demand = new Demand({
      user_email: req.body.user_email,
      message: req.body.message,
      name: req.body.name,
      firstname: req.body.firstname,
      territoire_id: req.body.territoire_id,
      user_id: req.body.user_id,
    });

    await demand.save();
    res.status(201).json({ demand: demand });
  } catch (error) {
    res.status(401).json({ error });
  }
};

exports.getDemands = async (req, res) => {
  try {
    const demands = await Demand.find().sort({ date: -1 });
    console.log("exports.getDemands -> demands", demands);
    res.status(201).json({ demands: demands });
  } catch (error) {
    res.status(401).json({ error });
  }
};

exports.deleteDemandById = async (req, res) => {
  try {
    console.log("deletOneDemande");

    await Demand.deleteOne({
      _id: req.params.id,
    });
    const demands = await Demand.find().sort({ date: -1 });
    res.status(200).json({
      success: true,
      demands: demands,
      message: "demande supprimée",
    });
  } catch (error) {
    res.status(500).json({ error });
  }
};

exports.getUsers = async (req, res) => {
  try {
    const users = await User.find().sort({ name: 1 });

    res.status(200).json({ success: true, data: users });
  } catch (error) {
    res.status(400).json({ error });
  }
};

exports.getUserById = async (req, res) => {
  try {
    let user = await User.findOne({ _id: req.params.id });
    //on ne renvoie pas le mot de passe
    user.password = null;
    res.status(200).json({ user: user, success: true });
  } catch (error) {
    res.status(400).json({ error, success: false });
  }
};

exports.deleteUserById = async (req, res) => {
  try {
    await User.deleteOne({ _id: req.params.id });

    const users = await User.find().sort({ name: 1 });

    //on renvoie la nouvelle liste des Users
    res
      .status(200)
      .json({ success: true, users: users, message: "utilisateur supprimé" });
  } catch (error) {
    res.status(400).json({ error });
  }
};

exports.updateUserById = async (req, res) => {
  try {
    const user = await User.updateOne(
      { _id: req.params.id },

      { ...req.body }
    );

    const modifiedUser = await User.findOne({ _id: req.params.id });

    res.status(200).json({
      success: true,
      data: modifiedUser,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "server error" });
  }
};

exports.addUser = async (req, res) => {
  try {
    hash = await bcrypt.hash(req.body.password, 10);

    userToSave = new User({
      email: req.body.email,
      password: hash,
      name: req.body.name,
      firstname: req.body.firstname,
      admin: req.body.admin,
    });

    await userToSave.save();
    const users = await User.find().sort({ name: 1 });

    res
      .status(201)
      .json({ success: true, message: "utilisateur créé", users: users });
  } catch (error) {
    res.status(400).json({ success: false, error });
  }
};

exports.assignTerritoryToUserById = async (req, res) => {
  console.log("assign");

  try {
    let user = await User.findOne({ _id: req.params.id });
    console.log("exports.assignTerritoryToUserById -> user", user);
    const fullTerritory = await Territoire.findOne({
      _id: req.params.territoryId,
    });

    const territoryToAdd = {
      territoire_id: fullTerritory._id,
      territoireID: fullTerritory.territoireID,
      territoireName: fullTerritory.territoireName,
    };
    user.territories.unshift(territoryToAdd);

    await User.updateOne({ _id: user._id }, user);

    // update territory assignment
    await Territoire.updateOne({ _id: fullTerritory._id }, { assigned: true });

    res.status(201).json({ user });
  } catch (error) {
    res.status(400).json({ error });
  }
};

function searchTerritoryIdInList(list, id) {
  let index = 0;
  let found = false;
  list.forEach((territory) => {
    if (territory.territoireID == id) {
      found = true;
    } else {
      index += 1;
    }
  });
  return { found: found, index: index };
}

exports.returnTerritoryFromUserById = async (req, res) => {
  try {
    let user = await User.findOne({ _id: req.params.id });

    if (user) {
      const userTerritoriesList = user.territories;

      //recherche du territoire dans la liste et si oui à quel index
      const { found, index } = await searchTerritoryIdInList(
        userTerritoriesList,
        req.params.territoryId
      );
      console.log(found, index);

      if (found) {
        userTerritoriesList.splice(index, 1);

        user.territories = userTerritoriesList;

        await User.updateOne({ _id: user._id }, user);
        res.status(200).json({ user });
      } else {
        throw "le territoire n'existe pas";
      }
    } else {
      throw "l'utilisateur n'existe pas";
    }
  } catch (error) {
    res.json({ error });
  }
};

exports.getUserTerritories = async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.params.id });
    if (user) {
      res.json({ success: true, data: user.territories });
    }
  } catch (error) {
    res.json({ success: false, error });
  }
};
