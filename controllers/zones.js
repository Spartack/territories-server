const fs = require("fs");
const Territoire = require("../models/Territoire");

exports.getZones = async (req, res, next) => {
  fs.access("zones.json", (err) => {
    if (err) {
      // si le fichier n'existe pas on renvoie un tableau vide
      res.json({ zones: [] });
    } else {
      fs.readFile("zones.json", (err, zones) => {
        if (err) res.json({ success: false });
        res.json(JSON.parse(zones));
      });
    }
  });
};

exports.addZones = async (req, res) => {
  fs.writeFile("zones.json", JSON.stringify(req.body), (err) => {
    if (err) res.send({ success: false });
    res.send({ success: true });
  });
};

exports.deleteZone = async (req, res, next) => {
  const zones = JSON.stringify({ zones: req.body.zones });

  fs.writeFile("zones.json", zones, (err) => {
    if (err) throw err;
    res.send({ success: true });
  });

  try {
    const territoiresUpdated = await Territoire.updateMany(
      { zone: req.body.zoneToDelete },
      { $set: { zone: "" } }
    );
    res.json(territoiresUpdated);
  } catch (error) {
    res.json(err);
  }
};
