const express = require("express");
const {
  signup,
  login,
  demandTerritoire,
  getDemands,
  deleteDemandById,
  getUsers,
  deleteUserById,
  addUser,
  getUserById,
  updateUserById,
  assignTerritoryToUserById,
  returnTerritoryFromUserById,
  getUserTerritories,
} = require("../controllers/users");

const router = express.Router();

router.route("/demand/:id").delete(deleteDemandById);

router.route("/demand").post(demandTerritoire).get(getDemands);

router.route("/login").post(login);

router.route("/signup").post(signup);

router.route("/:id/territories").get(getUserTerritories);

router
  .route("/:id/:territoryId")
  .post(assignTerritoryToUserById)
  .delete(returnTerritoryFromUserById);

router
  .route("/:id")
  .delete(deleteUserById)
  .get(getUserById)
  .put(updateUserById);

router.route("/").get(getUsers).post(addUser);

module.exports = router;
