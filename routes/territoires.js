const express = require("express");
const {
  getTerritoires,
  addTerritoires,
  getTerritoireById,
  updateOneTerritoireById,
  deleteTerritoireById,
  deleteAllTerritoires,
  addOneTerritoire,
  getFreeTerritories,
} = require("../controllers/territoires");

const auth = require("../middlewares/Auth");

const { addZones, getZones, deleteZone } = require("../controllers/zones");

const router = express.Router();

router.route("/addOne").post(auth, addOneTerritoire);

router.route("/zones").post(addZones).get(getZones).delete(deleteZone);

router
  .route("/")
  .get(getTerritoires)
  .post(addTerritoires)
  .delete(deleteAllTerritoires);

router.route("/free").get(getFreeTerritories);

router
  .route("/:id")
  .get(getTerritoireById)
  .put(updateOneTerritoireById)
  .delete(deleteTerritoireById);

module.exports = router;
