const path = require("path");
const express = require("express");
const dotenv = require("dotenv");
const cors = require("cors");
const connectDB = require("./config/db");

// init var
dotenv.config({ path: "./config/config.env" });

//database connexion
connectDB();

const app = express();

//body parser
app.use(express.json());

//CORS
app.use(cors());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content, Accept, Content-Type, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, DELETE, PATCH, OPTIONS"
  );
  next();
});

//Static file
app.use(express.static(path.join(__dirname, "public")));

// Routes
app.use("/users", require("./routes/users"));
app.use("/API/territoires", require("./routes/territoires"));

const PORT = process.env.PORT || 8000;

app.listen(PORT, () => {
  console.log(`server running in ${process.env.NODE_ENV} mode on port ${PORT}`);
});
