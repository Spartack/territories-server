mapboxgl.accessToken =
  "pk.eyJ1Ijoic3BydGNrIiwiYSI6ImNrOWlqc252ZTAzbjEzbHA1MzhoOHEwMzIifQ.hM9kiu9wtEjuvZHpIDPdbA";

const polygon = turf.polygon([
  [
    [4.8288023471832275, 45.73521210986779],
    [4.829220771789551, 45.733594578499456],
    [4.832128286361694, 45.73388663624019],
    [4.83148455619812, 45.735137225190584],
    [4.8288023471832275, 45.73521210986779],
  ],
]);

const bbox = turf.bbox(polygon);
console.log(bbox);

var features = turf.featureCollection([polygon]);
const center = turf.center(features);
console.log(center.geometry.coordinates);

const map = new mapboxgl.Map({
  container: "map",
  style: "mapbox://styles/mapbox/streets-v11",
  center: center.geometry.coordinates,
  zoom: 17,
});

map.on("load", function () {
  map.addSource("maine", {
    type: "geojson",
    data: {
      type: "Feature",
      geometry: {
        type: "Polygon",
        coordinates: [
          [
            [4.8288023471832275, 45.73521210986779],
            [4.829220771789551, 45.733594578499456],
            [4.832128286361694, 45.73388663624019],
            [4.83148455619812, 45.735137225190584],
            [4.8288023471832275, 45.73521210986779],
          ],
        ],
      },
    },
  });
  map.addLayer({
    id: "maine",
    type: "fill",
    source: "maine",
    layout: {},
    paint: {
      "fill-color": "#088",
      "fill-opacity": 0.4,
    },
  });

  map.fitBounds(bbox, { padding: 20 });
});
