const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },

  password: {
    type: String,
    required: true,
  },

  name: {
    type: String,
    required: true,
  },

  firstname: {
    type: String,
    required: true,
  },

  admin: {
    type: Boolean,
    default: false,
  },

  territories: [
    {
      territoire_id: { type: String },
      territoireID: { type: String },
      territoireName: { type: String },
      assignedAt: { type: Date, default: Date.now },
    },
  ],
});

module.exports = mongoose.model("User", UserSchema);
