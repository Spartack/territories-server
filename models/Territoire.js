const mongoose = require("mongoose");

const TerritoireSchema = new mongoose.Schema({
  territoireID: {
    type: Number,
    required: true,
  },

  territoireName: {
    type: String,
    required: true,
  },

  coordinates: [[[Number]]],

  assigned: {
    type: Boolean,
    default: false,
  },

  numberBoites: {
    type: Number,
    default: 0,
  },

  zone: {
    type: String,
    default: "",
  },
});

module.exports = mongoose.model("Territoire", TerritoireSchema);
