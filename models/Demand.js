const mongoose = require("mongoose");

const DemandSchema = new mongoose.Schema({
  user_email: {
    type: String,
    required: true,
  },

  name: {
    type: String,
  },

  firstname: {
    type: String,
  },

  date: {
    type: Date,
    default: Date.now,
  },

  message: {
    type: String,
    required: true,
  },

  territoire_id: {
    type: String,
    default: null,
  },

  user_id: {
    type: String,
  },
});

module.exports = mongoose.model("Demand", DemandSchema);
